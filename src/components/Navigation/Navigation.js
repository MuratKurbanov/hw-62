import React from 'react';
import logo from "../image/logo.png";
import {NavLink} from "react-router-dom";

const Navigation = () => {
    return (
        <div className='header'>
            <div className='btn'>
                <a href="#">Mac</a>
                <a href="#">IPad</a>
                <a href="#">IPhone</a>
                <a href="#">Watch</a>
                <a href="#">TV</a>
                <a href="#">Music</a>
            </div>
            <div className='logo'>
                <img src={logo} alt="#" className='apple'/>
            </div>
            <div className='link'>
                <NavLink to='/Home' className='links'>Home</NavLink>
                <NavLink to='/AboutUs' className='links'>About us</NavLink>
                <NavLink to='/Contacts' className='links'>Contacts</NavLink>
            </div>
        </div>
    );
};

export default Navigation;