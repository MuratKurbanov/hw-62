import React from 'react';
import Navigation from '../Navigation/Navigation';
import xs from '../image/xs.png';
import ipad from '../image/ipad.jpeg';
import macbook from '../image/macbook.png';
import apple from '../image/apple.png';
import watch from '../image/watch.jpg';
import airpods from '../image/airpods.jpg';

const Home = () => {
    return (
        <div>
            <Navigation />
            <div className='macbook'>
                <h2>MacBook Pro</h2>
                <p>More power.</p>
                <p>More performance.</p>
                <p>More pro.</p>
                <div className='learn'>
                    <a href="#">Learn more ></a>
                    <a href="#">Buy ></a>
                </div>
                <img src={macbook} alt="#" className='mac'/>
            </div>
            <div className='ipad'>
                <h2>iPad Pro</h2>
                <p>All new. All screen. All powerful.</p>
                <div className='learn'>
                    <a href="#">Learn more ></a>
                    <a href="#">Buy ></a>
                </div>
                <img src={ipad} alt="#" className='pro'/>
            </div>
            <div className='iphone'>
                <h2>iPhone X s</h2>
                <p>Largest Super Retina display. Fastest performance with</p>
                <p>A 12 Bionic.Most secure facial authentication with Face ID.</p>
                <p>Breakthrough dual cameras with Depth Control.</p>
                <div className='learn'>
                    <a href="#">Learn more ></a>
                    <a href="#">Buy ></a>
                </div>
                <img src={xs} alt="#" className='xs'/>
            </div>
            <div className='accessory'>
                <div className='watch'>
                    <div className='aw'>
                        <img src={apple} alt="#"/>
                        <h2>Watch</h2>
                    </div>
                    <h4>Series 4</h4>
                    <p>All new. For a better you.</p>
                    <div className='learn'>
                        <a href="#">Learn more ></a>
                        <a href="#">Buy ></a>
                    </div>
                    <img src={watch} alt="#" className='awatch'/>
                </div>
                <div className="ap">
                    <div className='airpods'>
                        <h2>AirPods</h2>
                        <p>Wireless. Effortless. Magical.</p>
                        <div className='learn'>
                            <a href="#">Learn more ></a>
                            <a href="#">Buy ></a>
                        </div>
                        <img src={airpods} alt="#" className='pro'/>
                    </div>
                </div>
            </div>
        </div>

    );
};

export default Home;