import React from 'react';
import Navigation from '../Navigation/Navigation';
import logo from '../image/logo.png'

const AboutUs = () => {
    return (
        <div>
            <Navigation />
            <div className='inc'>
                <img src={logo} alt="#" className='comp'/>
                <h1>Apple Inc.</h1>
            </div>
            <div className='cont'>
                <p>Apple Inc. is an American multinational technology company headquartered in Cupertino, California, that designs, develops, and sells consumer electronics, computer software, and online services. It is considered one of the Big Four of technology along with Amazon, Google and Facebook.</p>
                <p>The company's hardware products include the iPhone smartphone, the iPad tablet computer, the Mac personal computer, the iPod portable media player, the Apple Watch smartwatch, the Apple TV digital media player, and the HomePod smart speaker. Apple's software includes the macOS and iOS operating systems, the iTunes media player, the Safari web browser, and the iLife and iWork creativity and productivity suites, as well as professional applications like Final Cut Pro, Logic Pro, and Xcode. Its online services include the iTunes Store, the iOS App Store and Mac App Store, Apple Music, and iCloud.</p>
                <p>Apple was founded by Steve Jobs, Steve Wozniak, and Ronald Wayne in April 1976 to develop and sell Wozniak's Apple I personal computer. It was incorporated as Apple Computer, Inc., in January 1977, and sales of its computers, including the Apple II, grew quickly. Within a few years, Jobs and Wozniak had hired a staff of computer designers and had a production line. Apple went public in 1980 to instant financial success. Over the next few years, Apple shipped new computers featuring innovative graphical user interfaces, such as the original Macintosh in 1984, and Apple's marketing commercials for its products received widespread critical acclaim. However, the high price tag of its products and limited software titles caused problems, as did power struggles between executives at the company. In 1985, Wozniak stepped away from Apple, while Jobs resigned and founded a new company—NeXT—with former Apple employees.</p>
                <p>As the market for personal computers increased, Apple's computers lost share to lower-priced products, particularly ones that ran the Microsoft Windows operating system, and the company was financially on the brink. After more executive job shuffles, CEO Gil Amelio in 1997 bought NeXT to bring Jobs back. Jobs regained leadership within the company and became the new CEO shortly after. He began to rebuild Apple's status, opening Apple's own retail stores in 2001, acquiring numerous companies to create a portfolio of software titles, and changing some of the hardware used in its computers. The company returned to profitability. In January 2007, Jobs renamed the company Apple Inc., reflecting its shifted focus toward consumer electronics, and announced the iPhone, which saw critical acclaim and significant financial success. In August 2011, Jobs resigned as CEO due to health complications, and Tim Cook became the new CEO. Two months later, Jobs died, marking the end of an era for the company.</p>
            </div>
        </div>
    );
};

export default AboutUs;