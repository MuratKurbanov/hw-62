import React from 'react';
import Navigation from '../Navigation/Navigation';

const Contacts = () => {
    return (
        <div>
            <Navigation />
            <div className='container'>
                <h2>Contact Apple for support and service</h2>
                <h4>See a list of Apple phone numbers around the world</h4>
                <p>Before you call, have your serial number ready. Or start your support request online and we'll connect you to an expert.</p>
                <p>Many phone numbers listed here only work when dialed from within their associated countries or regions. If your country or region is not listed, see your support options.</p>
                <h2>The United States and Canada</h2>
                <div className='usa'>
                    <div>
                        <h3>United States</h3>
                        <p>1-800-275-2273</p>
                        <h4>iPhone</h4>
                        <p>1-800-MY-IPHONE</p>
                        <p>(1-800-694-7466)</p>
                        <h4>Education customers</h4>
                        <p>Support: 1-800-800-2775</p>
                        <p>Sales: 1-800-780-5009</p>
                        <h4>Enterprise</h4>
                        <p>1-866-752-7753</p>
                        <h4>Accessibility and assistive technology</h4>
                        <p>1-877-204-3930</p>
                        <h4>App Store, iTunes Store, and iBooks Store billing and help</h4>
                        <h3>Contact Apple Support</h3>
                        <h4>Apple Pay Cash and person to person payments</h4>
                        <p>1-877-233-8552</p>
                    </div>
                    <div>
                        <h3>Canada (English)</h3>
                        <p>1-800-263-3394</p>
                        <h3>Canada (French)</h3>
                        <p>1-800-263-3394</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Contacts;