import React, { Component } from 'react';
import './App.css'
import Home from './components/Home/Home';
import AboutUs from './components/AboutUs/AboutUs';
import Contacts from './components/Contacts/Contacts';

import {BrowserRouter, Switch, Route} from "react-router-dom";
class App extends Component {
  render() {
    return (
        <BrowserRouter>
          <Switch>
            <Route path='/' exact component={Home} />
            <Route path='/Home' exact component={Home} />
            <Route path='/AboutUs' component={AboutUs} />
            <Route path='/Contacts' component={Contacts} />
          </Switch>
        </BrowserRouter>
    );
  }
}

export default App;
